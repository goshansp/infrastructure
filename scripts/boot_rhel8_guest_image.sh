#!/bin/bash
# boot kvm guest image rhel8 cloud-init
# source: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/composing_a_customized_rhel_system_image/preparing-and-deploying-kvm-guest-images-with-image-builder_composing-a-customized-rhel-system-image
# prepare: 
# 1. download rhel-8.7-x86_64-kvm.qcow2 from https://access.redhat.com/downloads/content/479/ver=/rhel---8/8.7/x86_64/product-software
# 2. $ sudo cp ~/Downloads/rhel-8.7-x86_64-kvm.qcow2 /var/lib/libvirt/images/rhel-aap-8.7-x86_64-kvm.qcow2
# 3. $ sudo genisoimage -output /var/lib/libvirt/images/cloud-init.iso -volid cidata -joliet -rock user-data meta-data
# 4. $ sudo vi /etc/nsswitch.conf # add libvirt to hosts:
# 5. $ sudo qemu-img resize /var/lib/libvirt/images/rhel-aap-8.7-x86_64-kvm.qcow2 +20G
# 6. $ add user libvirt
# 7. run this script # ./boot_rhel8_guest_image.sh
# 8. $ ssh rhel8-aap
# 9. $ sudo subscription-manager register --username rh-ee-hgosteli
# Cleanup: $ sudo virsh destroy rhel8; sudo virsh undefine rhel8
# Todo: do not stay in console after virt-install

# virt-install \
# --memory 16000 \
# --vcpus 4 \
# --name rhel8-aap \
# --disk /var/lib/libvirt/images/rhel-aap-8.7-x86_64-kvm.qcow2,device=disk,bus=virtio,format=qcow2 \
# --disk /var/lib/libvirt/images/cloud-init.iso,device=cdrom \
# --os-variant rhel8.7 \
# --virt-type kvm \
# --graphics none \
# --console pty,target_type=serial \
# --import \
# --connect qemu:///system

virt-install \
--memory 4000 \
--vcpus 4 \
--name rhel8-runner \
--disk /var/lib/libvirt/images/rhel-runner-8.7-x86_64-kvm.qcow2,device=disk,bus=virtio,format=qcow2 \
--disk /var/lib/libvirt/images/cloud-init.iso,device=cdrom \
--os-variant rhel8.7 \
--virt-type kvm \
--graphics none \
--console pty,target_type=serial \
--import \
--connect qemu:///system