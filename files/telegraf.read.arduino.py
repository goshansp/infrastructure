#!/usr/bin/python

# pip install pyserial

import serial
import time

def main():
    ser = serial.Serial('/dev/ttyUSB0',115200,timeout=1)
    time.sleep(2)
    ser.write(b'A')
    print(ser.readline().decode("utf-8"))
    ser.close()


if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
