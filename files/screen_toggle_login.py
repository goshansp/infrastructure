#!/usr/bin/python3

import dbus
# bus = dbus.SystemBus()
bus = dbus.SessionBus()

display_config_well_known_name = "org.gnome.Mutter.DisplayConfig"
display_config_object_path = "/org/gnome/Mutter/DisplayConfig"

display_config_proxy = bus.get_object(display_config_well_known_name, display_config_object_path)
display_config_interface = dbus.Interface(display_config_proxy, dbus_interface=display_config_well_known_name)

serial, physical_monitors, logical_monitors, properties = display_config_interface.GetCurrentState()

updated_logical_monitors = [
  dbus.Struct((dbus.Int32(0), dbus.Int32(0), dbus.Double(1.0), dbus.UInt32(0), dbus.Boolean(True), [dbus.Struct((dbus.String('HDMI-2'), dbus.String('1920x1080@60.000'), {}), signature=None)]), signature=None)
]

properties_to_apply = { "layout_mode": properties.get("layout-mode")}

method = 1 # 2 means show a prompt before applying settings; 1 means instantly apply settings without prompt

display_config_interface.ApplyMonitorsConfig(dbus.UInt32(serial), dbus.UInt32(method), updated_logical_monitors, properties_to_apply)