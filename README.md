# infrastructure

fully managed domestic infrastructure

## features
- patching of domestic infrastructure
- ssl certificate management letsencrypt
- workstation customizations
- deploy homeautomation (test, prod)
- deploy snapcast service
- deplooy pxe server

# bootstrap
Automated by fedora-silverblue.git systemd unit ansible-pull.service.

```
$ ansible-pull -U https://gitlab.com/goshansp/infrastructure.git -i localhost, bootstrap.yml

$ ssh-copy-id skywalker
$ ansible-playbook -i inventory.yml bootstrap.yml --limit skywalker --ask-become
```

# full configuration
```
$ ansible-playbook -i inventory.yml configuration.yml --limit skywalker --ask-become
```

# Update System
```
$ ansible-playbook -i inventory.yml ostree_maintenance.yml --limit trinity
```

## Getting started
```
$ python -m venv ~/.local/venv-molecule
$ source ~/.local/venv-molecule/bin/activate
$ pip install --upgrade pip
$ pip install -r requirements.txt
$ ansible-galaxy install --role-file roles/requirements.yml --force
```

# admin.yml
Deploys privileged configuration to rpis.
```
$ ansible-playbook -i inventory.yml admin.yml
```


# workload_rpi.yml
Deploys unprivileged/rootless stuff to service_user.
```
$ ansible-playbook -i inventory.yml --limit rpi03 workload_rpi.yml --user minion
```

# SSL debug
```
ansible-playbook -i inventory.yml --limit rpi03 ssl.yml

# manual cleanup
$ sudo rm /etc/ssl/singularity.pamperspoil.com
# delete cloudflare record
# retry 
```

## rate limit
```
fatal: [singularity]: FAILED! => {"changed": false, "msg": "Failed to start new order for https://acme-v02.api.letsencrypt.org/acme/new-order with status 429. Error urn:ietf:params:acme:error:rateLimited: \"Error creating new order :: too many failed authorizations recently: see https://letsencrypt.org/docs/failed-validation-limit/\".", "other": {"http_status": 429, "http_url": "https://acme-v02.api.letsencrypt.org/acme/new-order", "problem": {"detail": "Error creating new order :: too many failed authorizations recently: see https://letsencrypt.org/docs/failed-validation-limit/", "status": 429, "type": "urn:ietf:params:acme:error:rateLimited"}, "subproblems": []}}

https://letsencrypt.org/docs/failed-validation-limit/

-> 10:33 -> 5 failures per account, per hostname, per hour

```